<?php
    session_start();
    $_SESSION['current_course'] = null;
    if($_GET['debug_key'] == $conf['debug_key']){
        if($_GET['role'] == 'teacher'){
            echo file_get_contents('templates/teacher.html');
        }
        else if($_GET['role'] == 'student'){
            echo file_get_contents('templates/student.html');
        }
    }
    else{
        if(!isset($_SESSION['user_id'])){
            header( 'Location: login', true, 307 );
        }
        if(strrpos($_SESSION['user_data']['profile'],"teacher") !== false){
            echo file_get_contents('templates/teacher.html');
        }
        else if(strrpos($_SESSION['user_data']['profile'],"student") !== false){
            echo file_get_contents('templates/student.html');
        }


    }

    //echo "<pre>";
    //print_r($_SESSION);
?>