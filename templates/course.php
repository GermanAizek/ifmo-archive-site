<?php
    session_start();
    if(!isset($_SESSION['user_id'])){
        header( 'Location: ../login', true, 307 );
    }
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    if(is_numeric($url[1])){

        $course_id = $url[1];
        $_SESSION['current_course'] = $course_id;
        $query = "SELECT * FROM courses WHERE id = {$course_id}";
        $res = $db->query($query)->fetchAll()[0];
        if($res["deleted"] == 1){
            echo file_get_contents('templates/messages/course_deleted.html');
        }
        else if($res["active"] == 0){
            echo file_get_contents('templates/course.html');
        }
        else{
            echo file_get_contents('templates/course.html');
        }
    }

?>