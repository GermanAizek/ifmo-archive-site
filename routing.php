<?php
    $url = explode("/",$_GET['url']);
    switch ($url[0]){
        case "": include("templates/main.php"); break;
        case "login": include("templates/login.php"); break;
        case "course":
            if($url[1] == 'getData.php'){
                include("modules/course/getData.php");
            }
            else if($url[1] == 'setData.php'){
                include("modules/course/setData.php");
            }
            else{
                include("templates/course.php"); break;
            }
            break;
        case "users":
            if($url[1] == 'setData.php'){
                include("modules/users/setData.php");
            }
            else if($url[1] == 'getData.php'){
                include("modules/users/getData.php");
            }
            break;
        case "directory":
            if($url[1] == 'getData.php'){
                include("modules/directory/getData.php");
            }
            break;
        case "certificates":
            if($url[1] == 'setData.php'){
                include("modules/certificates/setData.php");
            }
            else if($url[1] == 'getData.php'){
                include("modules/certificates/getData.php");
            }
            else if($url[1] == 'upload.php'){
                include("modules/certificates/upload.php");
            }
            else{
                $_GET["method"] = "get_file_arrd";
                include("modules/certificates/getData.php");
            }
            break;
        case "auth": include("modules/auth.php"); break;
        default: print_r("not found");
    }
?>