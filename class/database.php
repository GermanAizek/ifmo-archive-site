<?php
    class Database{
        public function __constrict($host,$port,$username,$secret,$dbname){
            $this->host = $host;
            $this->port = $port;
            $this->username = $username;
            $this->dbname = $dbname;
            $this->db = new PDO("mssql:host=$host;dbname=$dbname", $username, $secret);
        }
        public function query($queryString){
            return $this->db->query($queryString)->fetchAll(PDO::FETCH_KEY_PAIR);
        }
    }
?>