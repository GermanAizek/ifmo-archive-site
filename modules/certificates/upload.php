<?php
    session_start();
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    if(is_uploaded_file($_FILES["file"]["tmp_name"])){
        if(array_search(
                end(explode('.',$_FILES["file"]["name"])),
                array('png','jpg','pdf')
            ) !== false){
            //move_uploaded_file($_FILES["file"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"].$conf['project_directory']."/upload/".translit($_FILES["file"]["name"]));
            if($_FILES['file']['size'] < 204800){
                $file_path = $_SERVER["DOCUMENT_ROOT"].$conf['project_directory']."/upload/certificates/";
                $user_id = $_SESSION['user_id'];
                $course_id = $_SESSION['current_course'];
                $query = "SELECT firstname, lastname, name as `group`  FROM users JOIN groups ON groups.group_id = users.group WHERE users.user_id = {$user_id}";
                $res = $db->query($query)->fetchAll();
                $file_name = translit($res[0]['lastname'])."_".translit($res[0]['firstname'])."_".translit($res[0]['group'])."_course".$course_id.".".end(explode('.',$_FILES["file"]["name"]));
                $file_path = $file_path.$file_name;
                $query = "SELECT * FROM file_uploads WHERE file_path = '{$file_path}' AND deleted = 0";
                $res = $db->query($query)->fetchAll();
                if(count($res) > 0){
                    echo json_encode(array("status" => "error", "msg" => "Повторная загрузка сертификата невозможна, обратитесь к администратору"));
                }
                else{
                    move_uploaded_file($_FILES["file"]["tmp_name"],$file_path);
                    $query = "INSERT INTO file_uploads (file_path, user, status, course) VALUES ('{$file_path}',{$user_id},'upload',{$course_id})";
                    $db->query($query);
                    echo json_encode(array("status" => "success", "msg" => "Файл успешно загружен"));
                }

            }
            else{
                echo json_encode(array("status" => "error", "msg" => "Объем загружаемого файла не должен превышать 200 МБ"));
            }

        }
        else{

        }
        //move_uploaded_file($_FILES["file"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"].$conf['project_directory']."/upload/".translit($_FILES["file"]["name"]));
    }
    else{
        echo 'false';
    }




function translit($str) {
    $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
    $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
    return str_replace($rus, $lat, $str);
}

    //print_r(file_get_contents($_FILES['file']['tmp_name']));

?>