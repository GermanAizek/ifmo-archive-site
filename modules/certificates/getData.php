<?php
session_start();
try {
    $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
        $conf['db']['username'],
        $conf['db']['secret'],
        array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ));

} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}


switch ($_GET["method"]){
    case "get_certificate":
        if(strpos($_SESSION["profile"],"teacher") == false && strpos($_SESSION["profile"],"admin") == false && $_SESSION["user_id"] != $_GET["user_id"]){
            echo json_encode(array("status" => "error", "msg" => "Вы не можете загрузить сертификат этого пользователя"));
        }
        else{
            $query = "SELECT file_path FROM file_uploads WHERE user = {$_GET["user_id"]} AND course = {$_GET["course_id"]} AND deleted = 0";
            $files = $db->query($query)->fetchAll();
            if(count($files) == 0){
                echo json_encode(array("status" => "error", "msg" => "Запрашиваемый вами файл отсутствует или был удален"));
            }
            else{
                $file =  $files[0]["file_path"];
                if (file_exists($file)) {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($file));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    ob_clean();
                    flush();
                    readfile($file);
                    exit;
                }
                else{
                    echo json_encode(array("status" => "error", "msg" => "Запрашиваемый вами файл отсутствует или был удален"));
                }
            }
        }
        break;
    case "get_list":
        $user_id = $_SESSION['user_id'];
        $query = "SELECT fu.upload_date, c.shortname, fu.status, c.id FROM file_uploads fu
            JOIN courses c ON c.id = fu.course
            WHERE fu.user = {$user_id} AND fu.deleted = 0";
        $files = $db->query($query)->fetchAll();
        echo json_encode(array("status" => "success", "data" => $files));
        break;
    default:
        echo json_encode(array("status" => "error", "msg" => "Метод не найден"));
        break;
}




?>