<?php
    session_start();
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    switch ($_POST['method']){
        case 'get_group_list':
            $course_list = implode(",",$_POST["course_list"]);
            $query = "SELECT group_id, name FROM groups".($course_list != "" ? (" WHERE course in (".$course_list.")"): "");
            $res = $db->query($query)->fetchAll();
            echo json_encode(array("status" => "success", "data" => $res));
            break;
        case 'getLessons':
            $query = "SELECT * FROM rbLessons WHERE deleted = 0 ORDER BY shortname";
            $res = $db->query($query)->fetchAll();
            echo json_encode(array("status" => "success", "data" => $res));
            break;
        default: echo json_encode(array("status" => "error", "msg" => "Метод не найден"));
    }
?>