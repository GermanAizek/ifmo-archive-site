<?php
    session_start();
    $url = "https://go.segrys.ru/api";
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    switch ($_POST['method']){
        case "updateTeachers":
            $result = file_get_contents($url."/teachers?".http_build_query(array(
                        "app_key" => $conf["app_key"]
                    )
                )
                , false, stream_context_create(array(
                    "http" => array(
                        "header" => "Content-type: application/x-www-form-urlencoded\r\ntoken: ".$_SESSION['token']."\r\n",
                        "method" => "GET"
                    )
                )));
            $result = json_decode($result,true);
            if ($result === FALSE) {
                echo json_encode(array(
                        "status" => "error",
                        "msg" => "Произошла ошибка, обратитесь к администратору",
                        "code" => 1101)
                );
                return;
            }
            else{
                if($result['result'] == 'error'){
                    echo json_encode(array(
                            "status" => "error",
                            "msg" => "Произошла ошибка, обратитесь к администратору",
                            "code" => 1102)
                    );
                    return;
                }
                else{
                    //==================Получаем список существующих преподавателей===============//
                    $teachers = $db->query("SELECT * from users WHERE profile LIKE '%teacher%'")->fetchAll();
                    $teacher_list = array();
                    foreach ($teachers as $item){
                        $teacher_list[] = $item['user_id'];
                    }
                    foreach ($result['teachers'] as $teacher){
                        if(!in_array($teacher['user_id'],$teacher_list)){
                            $result_userData = file_get_contents($url."/profile?".http_build_query(array(
                                        "user_id" => $teacher['user_id']
                                    )
                                )
                                , false, stream_context_create(array(
                                    "http" => array(
                                        "header" => "Content-type: application/x-www-form-urlencoded\r\ntoken: ".$_SESSION['token']."\r\n",
                                        "method" => "GET"
                                    )
                                )));
                            $result_userData = json_decode($result_userData,true);
                            if ($result_userData === FALSE) {
                                echo json_encode(array(
                                        "status" => "error",
                                        "msg" => "Произошла ошибка, обратитесь к администратору",
                                        "code" => 1103)
                                );
                                return;
                            }
                            else{
                                if($result_userData['result'] == 'error'){
                                    echo json_encode(array(
                                            "status" => "error",
                                            "msg" => "Произошла ошибка, обратитесь к администратору",
                                            "code" => 1104)
                                    );
                                    return;
                                }
                                else{
                                    $query = "SELECT * FROM users WHERE user_id = {$teacher['user_id']}";
                                    if(count($db->query($query)->fetchAll()) > 0){
                                        //==================Обновляем старого преподавателя==============//
                                        $query = "UPDATE users SET profile = CONCAT(profile,';teacher') WHERE user_id = {$teacher['user_id']}";
                                        $db->query($query);
                                    }
                                    else{
                                        //==================Создаем нового преподавателя==============//
                                        $query = "INSERT INTO users (
                                            user_id,
                                            firstname,
                                            lastname,
                                            patrname,
                                            image,
                                            phone,
                                            email,
                                            profile,
                                            `group`
                                            ) VALUES (
                                            {$teacher['user_id']},
                                            '{$result_userData['firstname']}',
                                            '{$result_userData['lastname']}',
                                            '{$result_userData['middlename']}',
                                            '{$result_userData['photo']}',
                                            '{$result_userData['phone']}',
                                            '{$result_userData['email']}',
                                            'teacher',
                                            null
                                            );
                                            ";
                                        $db->query($query);
                                    }



                                }
                            }
                        }
                    }
                    echo json_encode(array("status" => "success"));
                }
            }
            break;
        case "updateStudents":
            $course = $_POST['course'];
            $result = file_get_contents($url."/studentsList?".http_build_query(array(
                        "course" => $course
                    )
                )
                , false, stream_context_create(array(
                    "http" => array(
                        "header" => "Content-type: application/x-www-form-urlencoded\r\ntoken: ".$_SESSION['token']."\r\n",
                        "method" => "GET"
                    )
                )));
            $result = json_decode($result,true);

            if ($result === FALSE) {
                echo json_encode(array(
                        "status" => "error",
                        "msg" => "Произошла ошибка, обратитесь к администратору")
                );
            }
            else{
                if($result['result'] == 'error'){
                    echo json_encode(array(
                            "status" => "error",
                            "msg" => "Произошла ошибка, обратитесь к администратору")
                    );
                }
                else{
                    //==================Получаем список существующих групп===============//
                    $groups = $db->query("SELECT * from groups")->fetchAll();
                    $group_list = array();
                    foreach ($groups as $item){
                        $group_list[] = $item['group_id'];
                    }
                    //==================Получаем список существующих студентов===============//
                    $students = $db->query("SELECT * from users WHERE profile LIKE '%student%'")->fetchAll();
                    $student_list = array();
                    foreach ($students as $item){
                        $student_list[] = $item['user_id'];
                    }
                    //==================Проходим по списку полученных групп для определенного курса===============//
                    foreach($result['groups'] as $group){
                        if(!in_array($group['group_id'],$group_list)){
                            //==================Создаем группу, если она не присутсвует в базу===============//
                            $query = "INSERT INTO groups (group_id, name, course) VALUES ({$group['group_id']},'{$group['name']}',$course);";
                            $db->query($query);
                        }
                        //==================Проходим по списку полученных студентов в группе===============//
                        foreach ($group['students'] as $student){
                            if(!in_array($student['user_id'],$student_list)){
                                //==================Получаем инфо о студенте через API, если его нет в системе===============//
                                $result_userData = file_get_contents($url."/profile?".http_build_query(array(
                                            "user_id" => $student['user_id']
                                        )
                                    )
                                    , false, stream_context_create(array(
                                        "http" => array(
                                            "header" => "Content-type: application/x-www-form-urlencoded\r\ntoken: ".$_SESSION['token']."\r\n",
                                            "method" => "GET"
                                        )
                                    )));
                                $result_userData = json_decode($result_userData,true);
                                if ($result_userData === FALSE) {
                                    echo json_encode(array(
                                            "status" => "error",
                                            "msg" => "Произошла ошибка, обратитесь к администратору")
                                    );
                                }
                                else{
                                    if($result_userData['result'] == 'error'){
                                        echo json_encode(array(
                                                "status" => "error",
                                                "msg" => "Произошла ошибка, обратитесь к администратору")
                                        );
                                    }
                                    else{
                                        $query = "SELECT * FROM users WHERE user_id = {$teacher['user_id']}";
                                        if(count($db->query($query)->fetchAll()) > 0){
                                            //==================Обновляем старого студента==============//
                                            $query = "UPDATE users SET profile = CONCAT(profile,';student') WHERE user_id = {$teacher['user_id']}";
                                            $db->query($query);
                                        }
                                        else{
                                            //==================Создаем студента с прикрикреплением его в группу===============//
                                            $query = "INSERT INTO users (
                                            user_id,
                                            firstname,
                                            lastname,
                                            patrname,
                                            image,
                                            phone,
                                            email,
                                            profile,
                                            `group`
                                            ) VALUES (
                                            {$student['user_id']}, 
                                            '{$result_userData['firstname']}',
                                            '{$result_userData['lastname']}',
                                            '{$result_userData['middlename']}',
                                            '{$result_userData['photo']}',
                                            '{$result_userData['phone']}',
                                            '{$result_userData['email']}',
                                            'student',
                                            {$group['group_id']}
                                            );
                                            ";
                                            //echo $query;
                                            $db->query($query);
                                        }


                                    }
                                }
                            }
                        }
                    }
                    echo json_encode(array("status" => "success"));

                }
            }
            break;
        default: echo json_encode(array("status" => "error", "msg" => "Метод не найден"));
    }
?>