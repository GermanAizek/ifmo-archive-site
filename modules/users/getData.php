<?php
    session_start();
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    switch ($_POST['method']) {
        case "getMyData":
            $query = "SELECT * FROM users WHERE user_id = {$_SESSION['user_id']}";
            $user_data = $db->query($query)->fetchAll();
            echo json_encode(array("status" => "success", "data" => $user_data[0]));
            break;
        default: echo json_encode(array("status" => "error", "msg" => "Метод не найден"));
    }
?>