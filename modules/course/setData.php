<?php
    session_start();
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    switch ($_POST['method']) {
        case "enroll":
            $query = "SELECT * FROM course_members WHERE course = {$_POST['course_id']} AND user = {$_SESSION['user_id']} AND role = 'member' AND deleted = 0";
            $res = $db->query($query)->fetchAll();
            if(count($res)!= 0){
                echo json_encode(array("status" => "error", "msg" => "Вы уже записаны на данный курс"));
            }
            else{
                $query = "INSERT INTO course_members (course, user, role) VALUES ({$_POST['course_id']}, {$_SESSION['user_id']}, 'member')";
                $res = $db->query($query);
                echo json_encode(array("status" => "success", "msg" => "Вы успешно записались на курс"));
            }
            break;
        case "enroll_head":
            $query = "SELECT * FROM course_members WHERE course = {$_POST['course_id']} AND user = {$_SESSION['user_id']} AND role = 'head' AND deleted = 0";
            $res = $db->query($query)->fetchAll();
            if(count($res)!= 0){
                echo json_encode(array("status" => "error", "msg" => "Вы уже записаны на данный курс"));
            }
            else{
                $query = "SELECT * FROM users WHERE user_id = {$_SESSION['user_id']} AND profile LIKE '%teacher%'";
                $res = $db->query($query)->fetchAll();
                if(count($res)== 0){
                    echo json_encode(array("status" => "error", "msg" => "Ах ты маленький бунтарь! Плохо притворяться преподом "));
                }
                else{
                    $query = "INSERT INTO course_members (course, user, role) VALUES ({$_POST['course_id']}, {$_SESSION['user_id']}, 'head')";
                    $res = $db->query($query);
                    echo json_encode(array("status" => "success", "msg" => "Вы успешно стали ведущим курса"));
                }

            }
            break;
        case "setRating":
            $query = "UPDATE course_ratings SET deleted = 1 WHERE course_id = {$_SESSION['current_course']} AND user_id = {$_SESSION['user_id']}";
            $db->query($query);
            $query = "INSERT INTO course_ratings (course_id,user_id,rating) VALUES ({$_SESSION['current_course']},{$_SESSION['user_id']},{$_POST['rating']})";
            echo $query;
            $res = $db->query($query);
            echo json_encode(array("status" => "success", "msg" => "Благодарим за оценку курса"));
            break;
        default: echo json_encode(array("status" => "error", "msg" => "Метод не найден"));
    }


?>