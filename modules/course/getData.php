<?php
    session_start();
    try {
         $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }

    switch ($_POST['method']){
        case "getCourses":
            $settings = get_object_vars(json_decode($_POST['settings']));
            $query = "SELECT c.*, AVG(cr.rating) AS rating, cm.count AS count_members, l.shortname as lesson_name
                        FROM courses AS c 
                        LEFT JOIN course_ratings AS cr ON cr.course_id = c.id AND cr.deleted = 0
                        LEFT JOIN rbLessons AS l ON l.id = c.lesson AND l.deleted = 0 
                        ".($settings['onlyMy'] == true ? "JOIN course_members AS cm2 ON cm2.course = c.id AND cm2.role = 'member' AND cm2.deleted = 0 AND cm2.user = {$_SESSION['user_id']}" : "")."
                        LEFT JOIN (SELECT course, COUNT(*) AS count FROM course_members WHERE deleted = 0 AND role = 'member' GROUP BY course) AS cm ON cm.course = c.id
                        WHERE c.deleted = 0 AND active = 1 
                        ".($settings['lesson'] != 0 ? (" AND l.id = ".$settings['lesson'] ) : "")." 
                        ".($settings['name'] != "" ? (" AND c.shortname like '%".$settings['lesson']."%'" ) : "")." 
                        ".($settings['old'] != true ? (" AND c.dateend >= now()" ) : "")." 
                        GROUP BY c.id 
                        ".($settings['sort'] != "0" ? (" ORDER BY ".$settings['sort'])." DESC" : "")." 
                        LIMIT {$_POST['pos']},{$_POST['count']}";

            $query_count = "SELECT count(*) AS count FROM (".substr($query,0, strpos($query,"LIMIT {$_POST['pos']},{$_POST['count']}")).") AS c";
            $query_partic_course = "SELECT course FROM course_members WHERE user = {$_SESSION['user_id']} AND deleted = 0";
            $query_head_course = "SELECT u.id, u.firstname, u.lastname, u.patrname, a.course
                                    FROM (SELECT cm.* 
                                          FROM course_members AS cm , ({$query}) AS a 
                                          WHERE a.id = cm.course AND role = 'head')
                                    AS a JOIN users AS u ON u.user_id = a.user";
            $res = $db->query($query)->fetchAll();
            $count = $db->query($query_count)->fetchAll();
            $partic_course = $db->query($query_partic_course)->fetchAll();
            $head_course = $db->query($query_head_course)->fetchAll();
            echo json_encode(array(
                "status" => "success",
                "data" => $res,
                "count" => $count[0]['count'],
                "partic_course" => $partic_course,
                "head_course" => $head_course
            ));
            break;
        case "getCourse":
            $course_id = $_POST['id'];
            $query = "SELECT c.*, AVG(cr.rating) AS rating, cm.count AS count_members, l.shortname AS lesson_name, u.name AS university_name, p.name AS platform_name, p.link AS platform_link
                        FROM courses AS c 
                        LEFT JOIN course_ratings AS cr ON cr.course_id = c.id AND cr.deleted = 0
                        LEFT JOIN rbLessons AS l ON l.id = c.lesson AND l.deleted = 0 
                        LEFT JOIN rbPlatforms AS p ON p.id = c.platform AND p.deleted = 0 
                        LEFT JOIN rbUniversity AS u ON u.id = c.university AND u.deleted = 0 
                        LEFT JOIN (SELECT course, COUNT(*) AS count FROM course_members WHERE deleted = 0 AND role = 'member' GROUP BY course) AS cm ON cm.course = c.id
                        WHERE c.id = {$course_id}";
            $query_сourse_members = "SELECT u.*, cm.role AS cm_role, g.name AS `group` FROM course_members AS cm 
                JOIN users AS u ON u.user_id = cm.user
                LEFT JOIN groups AS g ON g.group_id = u.`group`
                WHERE cm.course = {$course_id} AND cm.deleted = 0";
            $query_course_certificates = "SELECT * FROM file_uploads WHERE course = {$course_id} AND user = {$_SESSION['user_id']} AND status in ('upload','cancel','confirm') AND deleted = 0";
            $query_course_rating = "SELECT rating FROM course_ratings WHERE course_id = {$course_id} AND user_id = {$_SESSION['user_id']} AND deleted = 0";
            $course_rating = $db->query($query_course_rating)->fetchAll()[0];
            $course_certificates = $db->query($query_course_certificates)->fetchAll()[0];
            $course_info = $db->query($query)->fetchAll()[0];
            $сourse_members = $db->query($query_сourse_members)->fetchAll();
            echo json_encode(array(
                "status" => "success",
                "course_info" => $course_info,
                "сourse_members" => $сourse_members,
                "course_certificates" => $course_certificates,
                "course_rating" => $course_rating,
            ));
            break;
        case "get_statistic":
            $query_courses = "SELECT id, shortname FROM courses WHERE deleted = 0 ORDER BY id;";
            if(count($_POST["groups"]) > 0){
                $groups = implode(",",$_POST["groups"]);
                $query_users = "SELECT user_id, firstname,lastname,patrname FROM users WHERE profile like '%student%' AND `group` IN ($groups) ORDER BY lastname, firstname, patrname;";
            }
            else if(count($_POST["courses"]) > 0){
                $courses = implode(",",$_POST["courses"]);
                $query_users = "SELECT user_id, firstname,lastname,patrname FROM users WHERE profile like '%student%' AND `group` IN (SELECT id FROM groups WHERE course in ($courses)) ORDER BY lastname, firstname, patrname;";
            }
            else{
                $query_users = "SELECT user_id, firstname,lastname,patrname FROM users WHERE profile like '%student%' ORDER BY lastname, firstname, patrname";
            }
            $query_members = "SELECT id, course, user FROM course_members WHERE role = 'member' AND deleted = 0;";
            $courses = $db->query($query_courses)->fetchAll();
            $users = $db->query($query_users)->fetchAll();
            $сourse_members = $db->query($query_members)->fetchAll();
            echo json_encode(array("status" => "success", "data" => array(
                "courses" => $courses,
                "users" => $users,
                "сourse_members" => $сourse_members
            )));
            break;
        default: echo json_encode(array("status" => "error", "msg" => "Метод не найден"));
    }
    //print_r(get_class_methods('Database'));

?>