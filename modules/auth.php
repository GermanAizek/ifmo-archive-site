<?php
    session_start();
    if($_POST['method']=='logout'){
        session_destroy();
        return;
    }
    try {
        $db = new PDO("mysql:host={$conf['db']['host']};dbname={$conf['db']['db']};charset=UTF8",
            $conf['db']['username'],
            $conf['db']['secret'],
            array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ));

    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    $url = "https://go.segrys.ru/api";
    $result = file_get_contents($url."/authorization", false, stream_context_create(array(
        "http" => array(
            "header" => "Content-type: application/x-www-form-urlencoded",
            "method" => "POST",
            "content" => http_build_query(array(
                "login" => $_POST['login'],
                "password" => $_POST['secret'],
                "app_key" => $conf["app_key"]
            ))
        )
    )));
    if ($result === FALSE) {
        echo json_encode(array(
            "status" => "error",
            "msg" => "Произошла ошибка, обратитесь к администратору")
        );
    }
    else{
        $result = json_decode($result, true);
        if($result["result"] == "error"){
            if($result["error_code"] == 6){
                echo json_encode(array(
                        "status" => "error",
                        "msg" => "Неверный логин или пароль")
                );
            }
            else{
                echo json_encode(array(
                        "status" => "error",
                        "msg" => "Произошла ошибка, обратитесь к администратору")
                );
            }
        }
        else{
            $_SESSION['token'] = $result['token'];
            $_SESSION['user_id'] = $result['user_id'];
            $result = file_get_contents($url."/profile?".http_build_query(array(
                        "user_id" => $_SESSION['user_id']
                    )
                )
                , false, stream_context_create(array(
                "http" => array(
                    "header" => "Content-type: application/x-www-form-urlencoded\r\ntoken: ".$_SESSION['token']."\r\n",
                    "method" => "GET"
                )
            )));
            $result = json_decode($result,true);
            if ($result === FALSE) {
                echo json_encode(array(
                        "status" => "error",
                        "msg" => "Произошла ошибка, обратитесь к администратору")
                );
            }
            else{
                if($result['result'] == 'error'){
                    echo json_encode(array(
                            "status" => "error",
                            "msg" => "Произошла ошибка, обратитесь к администратору")
                    );
                }
                else{
                    $query = "SELECT * FROM users WHERE user_id = {$_SESSION['user_id']}";
                    $_SESSION['user_data'] = $db->query($query)->fetchAll()[0];
                    echo json_encode(array(
                            "status" => "success"
                        )
                    );
                    /*
                    $result = file_get_contents($url."/roles?".http_build_query(array(
                                "user_id" => $_SESSION['user_id']
                            )
                        )
                        , false, stream_context_create(array(
                            "http" => array(
                                "header" => "Content-type: application/x-www-form-urlencoded\r\ntoken: ".$_SESSION['token']."\r\n",
                                "method" => "GET"
                            )
                        )));
                    $result = json_decode($result,true);
                    if ($result === FALSE) {
                        echo json_encode(array(
                                "status" => "error",
                                "msg" => "Произошла ошибка, обратитесь к администратору")
                        );
                    }
                    else{
                        if($result['result'] == 'error'){
                            echo json_encode(array(
                                    "status" => "error",
                                    "msg" => "Произошла ошибка, обратитесь к администратору")
                            );
                        }
                        else{
                            $_SESSION['user_data']['role'] = $result;
                            echo json_encode(array(
                                    "status" => "success"
                                )
                            );
                        }
                    }
                    */
                }
            }
        }

    }

?>