$(window).ready(function () {
    $('[data-tab=statistic] .course_list').dropdown({
       onChange: function (value, text, $choice) {
           group_list_refresh(value);
       }
    });
    group_list_refresh([]);
    function group_list_refresh(value) {
        $.ajax({
            url: "directory/getData.php",
            type: "POST",
            data: {"method": "get_group_list", "course_list": value},
            success: function (data) {
                data = JSON.parse(data);
                $('[data-tab=statistic] .group_list select').html("");
                data.data.map(function (group) {
                    $('[data-tab=statistic] .group_list select').append("<option value='"+group.group_id+"'>"+group.name+"</option>");
                });
                $('[data-tab=statistic] .group_list').dropdown("clear");
                $('[data-tab=statistic] .group_list').dropdown("refresh");
            }
        })
    }
    $('[data-tab=statistic] .statistic_create').click(function () {
        groups = $('[data-tab=statistic] .group_list').dropdown("get value");
        courses = $('[data-tab=statistic] .course_list').dropdown("get value");
        $.ajax({
            url: "course/getData.php",
            type: "POST",
            data: {"method": "get_statistic", "courses": courses, "groups": groups},
            success: function (data) {
                data = JSON.parse(data);
                data = data.data;
                $('[data-tab=statistic] .accordion').html("");
                data.courses.map(function (course) {
                    $('[data-tab=statistic] .accordion').append("" +
                        "<div class='title' course_id='"+course.id+"'>" +
                            "<i class='dropdown icon'></i>"+ course.shortname +
                        "</div>" +
                        "<div class='content' course_id='"+course.id+"'>" +
                            "<table class='ui celled table'>" +
                                "<thead>" +
                                    "<tr>" +
                                        "<th>ФИО</th>" +
                                        "<th>Проходит</th>" +
                                        "<th>Сертификат</th>" +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>" +
                                "</tbody>" +
                            "</table>" +
                        "</div>" +
                        "");

                });
                data.users.map(function (user) {
                    $('[data-tab=statistic] .accordion .content table tbody').append("<tr user_id='"+user.user_id+"'>" +
                        "<td>"+user.lastname+" "+user.firstname[0]+". "+user.patrname[0]+".</td>" +
                        "<td><!--<i class='icon remove'></i>--></td>" +
                        "<td><!--<i class='icon remove'></i>--></td>" +
                        "</tr>")
                });
                data.сourse_members.map(function (course_member) {
                    $("[data-tab=statistic] .accordion .content[course_id="+course_member.course+"] tr[user_id="+course_member.user+"] td").eq(1).html("<i class='icon checkmark'></i>")
                });
                $('[data-tab=statistic] .accordion').accordion();

            }
        })
    })
});