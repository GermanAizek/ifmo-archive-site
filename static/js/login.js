$(window).ready(function () {
    $('.ui.checkbox').checkbox();
    $(window).keypress(function (event) {
        if(event.keyCode==13){
            $('.send').trigger('click');
        }
    });
    $('.send').click(function (event) {
        login = $('[name=login]').val();
        secret = $('[name=secret]').val();
        remember = $('.ui.checkbox').checkbox('is checked');
        load_start()
        $.ajax({
            url: 'auth/',
            type: 'POST',
            data: {login: login, secret: secret, remember: remember},
            success: function (data) {
                load_end();
                data = JSON.parse(data);
                if(data.status == "error"){
                    $.notify(data.msg, "error");
                }
                else if(data.status == "success"){
                    location.href = window.location.pathname.replace("login","");
                }
                else{
                    $.notify("Произошла ошибка, обратитесь к администратору", "error");
                }
            },
            error: function (data) {
                load_end();
                $.notify("Произошла ошибка, обратитесь к администратору", "error");
            }
        })
    })
});