$(window).ready(function () {
    var parser = document.createElement('a');
    parser.href = location.href;
    course_id = parser.pathname.split("/")[parser.pathname.split("/").length - 1];
    $('.logout').click(function () {
        $.ajax({
            url: "auth/",
            type: "POST",
            data: {"method": "logout"},
            success: function (data) {
                location.href = "../login";
            }
        })
    });
    $.ajax({
        url: "../users/getData.php",
        type: "POST",
        data: {"method": "getMyData"},
        success: function (data) {
            data = JSON.parse(data);
            user_data = data.data;
            $.ajax({
                url: "../course/getData.php",
                type: "POST",
                data: {"method": "getCourse", "id": course_id},
                success: function (data) {

                    data = JSON.parse(data);
                    is_member = false;
                    is_head = false;
                    data.сourse_members.map(function (user) {
                        if(user.user_id == user_data.user_id && user.cm_role == 'member'){
                            is_member = true;
                        }
                        else if(user.user_id == user_data.user_id && user.cm_role == 'head'){
                            is_head = true;
                        }
                        html = "<div class='item'>" +
                                "<img class='ui avatar image' src='"+user.image+"'>" +
                                "<div class='content'>" +
                                    "<a class='header' href='https://go.segrys.ru/profile/"+user.user_id+"'>"+user.lastname+" "+user.firstname[0]+"."+user.patrname[0]+"</a>" +
                                    "<div class='description'>"+(user.group != null ? user.group : "")+"</div>" +
                                "</div>" +
                            "</div>";
                        if(user.cm_role == 'member'){
                            $('.members_list').append(html);
                        }
                        else{
                            $('.head_list').append(html);
                        }
                    });
                    course = data.course_info;
                    if(course.active == 0){
                        $('.messages').html("<div class='ui icon message'>" +
                                "<i class='warning orange sign icon'></i>" +
                                "<div class='content'>" +
                                    "<div class='ui header red'>Внимание!</div>" +
                                    "<p>Выбранный вами курс неактивен</p>" +
                                "</div>" +
                            "</div>")
                    }
                    else{
                        //$('table.header').after("<div class='ui divider'></div>")
                    }
                    buttons = "";
                    if(!is_member){
                        buttons += "<div class='ui labeled button enroll_member'>" +
                            "<div class='ui blue button'><i class='add user icon'></i>Записаться</div>" +
                            "<div class='ui basic blue left pointing label'>"+$('.members_list').children().length+"</div>" +
                            "</div>";
                    }
                    else{
                        //buttons += "<div><label for='file' class='ui icon button'><i class='file icon'>Загрузить сертификат</i></label><input type='file' id='file' style='display: none'></div>"
                        buttons+= "<div class='ui button icon green add_certificate'><i class='icon file'></i>Загрузить сертификат</div>" +
                            "<form enctype='multipart/form-data' method='post'>" +
                            "<input type='file' name='file' class='add_certificate_input' style='display: none'>" +
                            "</form>"
                    }
                    if(!is_head && user_data.profile.indexOf('teacher')  != -1){
                        buttons += "<div class='ui labeled button enroll_head'>" +
                            "<div class='ui teal button'><i class='add user icon'></i>Вести курс</div>" +
                            "<div class='ui basic teal left pointing label'>"+$('.head_list').children().length+"</div>" +
                            "</div>";
                    }
                    $('.course_info_content').html("" +
                        "<img src='"+course.image+"' class='ui small bordered image course_image'>" +
                        "<h2 class='ui blue header'>"+course.fullname+" ("+course.lesson_name+")</h2>" +
                        "<table class='meta'>" +
                            "<tr>" +
                                "<td><i class='icon large clock'></i></td>" +
                                "<td>"+$.format.date(course.datestart,"dd.MM.yyyy") + " - " + $.format.date(course.dateend,"dd.MM.yyyy")+"</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td><i class='icon large student'></i></td>" +
                                "<td>"+course.university_name+"</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td><i class='icon large browser'></i></td>" +
                                "<td><a href='"+course.platform_link+"'>"+course.platform_name+"</a><a class='ui blue label' href='"+course.link+"' style='margin-left: 10px'>Перейти к курсу</a></td>" +
                            "</tr>" +
                        "</table>" +
                        "<div class='description' style='margin-top: 15px; text-align: justify;'>"+course. description+"</div>"+
                        "<div class='buttons'>"+buttons+"</div>" +
                        "");
                    if(data.course_certificates != null){
                        $('.add_certificate').replaceWith("<div class='ui large teal label'>" +
                            "<i class='archive icon'></i>" +
                            "Сертификат загружен: "+$.format.date(data.course_certificates.upload_date,"dd.MM.yyyy")+"" +
                            "</div>" +
                            "<div style='margin-top: 10px;'>" +
                            "<b>Оценить курс:</b> " +
                            "<div class='ui star large rating' data-max-rating='5' "+(data.course_rating != null ? ("data-rating='"+data.course_rating.rating+"'"): "")+"></div>" +
                            "</div>");
                        $('.ui.rating').rating({onRate: function (value) {
                            $.ajax({
                                url: "../course/setData.php",
                                type: "POST",
                                data: {"method": "setRating", "rating": value},
                                success: function (data) {
                                    data = JSON.parse(data);
                                    if (data.status == "error") {
                                        $.notify(data.msg, "error");
                                    }
                                    else if (data.status == "success") {

                                    }
                                },
                                error: function (data) {
                                    load_end();
                                    $.notify("Произошла ошибка, обратитесь к администратору", "error");
                                }
                            })
                        }});

                    }
                    $('.add_certificate_input').change(function () {
                       formData = new FormData($(this).parent()[0]);
                       formData['method'] = 'upload';
                       $.ajax({
                           url: "../certificates/upload.php",
                           type: "POST",
                           data: formData,
                           cache: false,
                           contentType: false,
                           processData: false,
                           success: function (data) {
                               data = JSON.parse(data);
                               if (data.status == "error") {
                                   $.notify(data.msg, "error");
                               }
                               else if (data.status == "success") {
                                   location.reload();
                               }
                           },
                           error: function (data) {
                               load_end();
                               $.notify("Произошла ошибка, обратитесь к администратору", "error");
                           }
                       })


                    });
                    $('.add_certificate').unbind('click').click(function () {
                        $('.add_certificate_input').trigger('click');
                    });
                    $('.enroll_member').unbind('click').click(function () {
                        $.ajax({
                            url: "../course/setData.php",
                            type: "POST",
                            data: {"method": "enroll", "course_id": course_id},
                            success: function (data) {
                                data = JSON.parse(data);
                                if (data.status == "error") {
                                    $.notify(data.msg, "error");
                                }
                                else if (data.status == "success") {
                                    location.reload();
                                }
                            },
                            error: function (data) {
                                load_end();
                                $.notify("Произошла ошибка, обратитесь к администратору", "error");
                            }
                        })
                    });
                    $('.enroll_head').unbind('click').click(function () {
                        $.ajax({
                            url: "../course/setData.php",
                            type: "POST",
                            data: {"method": "enroll_head", "course_id": course_id},
                            success: function (data) {
                                data = JSON.parse(data);
                                if (data.status == "error") {
                                    $.notify(data.msg, "error");
                                }
                                else if (data.status == "success") {

                                    location.reload();
                                }
                            },
                            error: function (data) {
                                load_end();
                                $.notify("Произошла ошибка, обратитесь к администратору", "error");
                            }
                        })
                    });



                }
            })
        }
    });
});

function getDayDelta(incomingDate, todayDate){
    var incomingDate = new Date(incomingDate),
        today = new Date(todayDate), delta;
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);

    today.setHours(0);
    today.setMinutes(0);

    delta = incomingDate - today;

    return Math.round(delta / 1000 / 60 / 60/ 24);
}