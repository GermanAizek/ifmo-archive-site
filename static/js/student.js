user_data = new Object();
$.ajax({
    url: "users/getData.php",
    type: "POST",
    data: {"method": "getMyData"},
    success: function (data) {
        data = JSON.parse(data);
        user_data = data.data;
    }
});


$(window).ready(function () {
    $('.menu .item').tab();
    $('.ui.dropdown').dropdown();
    $('.logout').click(function () {
        $.ajax({
            url: "auth/",
            type: "POST",
            data: {"method": "logout"},
            success: function (data) {
                location.href = "login";
            }
        })
    });


    //Список сертификатов
    $.ajax({
        url: "certificates/getData.php",
        type: "GET",
        data: {"method": "get_list"},
        success: function (data) {
            data = JSON.parse(data);
            data.data.map(function (file) {
                icon = "";
                switch (file.status){
                    case "upload": icon = "archive"; break;
                    case "submit": icon = "checkmark"; break;
                    case "cancel": icon = "ban"; break;
                }

                $('[data-tab=certificates] .certificates_list').append("" +
                    "<div class='item'>" +
                        "<i class='big "+icon+" middle aligned icon'></i>" +
                        "<div class='content'>" +
                            "<a class='header' href='certificates/getData.php?method=get_certificate&user_id="+user_data.user_id+"&course_id="+file.id+"'>"+file.shortname+"</a>" +
                            "<div class='description'>Загружен: "+$.format.date(file.upload_date,"dd.MM.yyyy")+"</div>" +
                        "</div>" +
                    "</div>")
            })
        }
    });

    //Список курсов
    if($.session.get("pos") == undefined){pos = 0; $.session.set("pos",0);} else{
        pos = $.session.get("pos");
    }
    if($.session.get("count") == undefined){count = 10;$.session.set("count",10);} else{
        count = $.session.get("count");
        $('[data-tab=course] .item_in_page').dropdown('set selected',count);
    }
    if($.cookie('course_settings') == undefined){
        course_settings = {
            sort: 0,
            lesson: 0,
            name: '',
            onlyMy: false,
            old: false
        };
        $.cookie('course_settings',JSON.stringify(course_settings));
    }
    else{
        course_settings = $.cookie('course_settings');
        course_settings = JSON.parse(course_settings);
    }

    course_view(pos, count);
    function course_view(pos, count) {
        $.ajax({
            url: "course/getData.php",
            type: "POST",
            data: {"method": "getCourses", "pos": pos, "count": count, "settings": JSON.stringify(course_settings)},
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == "error") {
                    $.notify(data.msg, "error");
                }
                else if (data.status == "success") {
                    partic_course = [];
                    data.partic_course.map(function (course) {
                        partic_course.push(course.course);
                    });
                    $('[data-tab=course] .ui.table tbody').html("");
                    data.data.map(function (course) {
                        button_element = "";
                        if(partic_course.indexOf(course.id) == -1){
                            button_element = "<div class='ui basic green button enroll' course_id='"+course.id+"'>Записаться</div>";
                        }
                        else{
                            button_element = "<div class='ui basic blue disabled button enroll' course_id='"+course.id+"'>Вы уже записаны</div>";
                        }
                        head_element = "";
                        data.head_course.map(function (row) {
                            if(row.course == course.id){
                                head_element += "<a class='ui blue label' user_id='"+row.id+"'>"+row.lastname+" "+row.firstname[0]+"."+row.patrname[0]+".</a>";
                            }
                        });
                        $('[data-tab=course] .ui.table tbody').append("<tr>" +
                                "<td><a href='course/" + course.id + "'><img src='" + course.image + "'></a></td>" +
                                "<td>" +
                                    "<h3><a href='course/" + course.id + "'>" + course.shortname + " ("+course.lesson_name+")</a></h3>" +
                                    "<p class='description'>" + course.shortdecription + " " + course.shortdecription + "</p>" +
                                    "<div class='meta'>" +
                                        "<span class='date'>" + $.format.date(course.datestart,"dd.MM.yyyy") + " - " + $.format.date(course.dateend,"dd.MM.yyyy") + "</span>" +
                                        "<span class='teachers'>"+head_element+"</span>" +
                                        "<div class='members'>" +
                                            "<div class='ui right pointing label'>" +
                                                ""+(course.count_members == null ? 0 : course.count_members)+" "+
                                                declOfNum((course.count_members == null ? 0 : course.count_members), ['участник', 'участника', 'участников'])+
                                            "</div>"+button_element+"" +
                                        "</div>" +
                                        "<div class='ui star rating' data-rating='"+Math.round(course.rating)+"'></div>" +
                                    "</div>" +
                                "</td>" +
                            "</tr>");
                    });
                    if(data.data.length == 0){
                        $('[data-tab=course] .ui.table tbody').append("<tr><td colspan='2'><h2>По вашему запросу ничего не найдено</h2></td></tr>")
                    }

                    $('.rating').rating({maxRating: 5}).rating('disable');
                    pageCount = parseInt(data.count / count) + 1;
                    position = parseInt(pos / count) + 1;
                    $('[data-tab=course] .pagination').html("<a class='item prev " + (position == 1 ? "disabled" : "") + "'><i class='icon chevron left'></i></a>" +
                        generatePages(pageCount, position) +
                        "<a class='item next " + (position == pageCount ? "disabled" : "") + "'><i class='icon chevron right'></i></a>"
                    );
                    $('.pagination .item').click(function () {
                        if($(this).hasClass('disabled')){
                            return;
                        }
                        if($(this).hasClass('prev')){
                            pos = (parseInt($('.pagination .item.active').text())-2)*count;
                            $.session.set('pos',pos);
                            course_view(pos, count, []);
                        }
                        else if($(this).hasClass('next')){
                            pos = parseInt($('.pagination .item.active').text())*count;
                            $.session.set('pos',pos);
                            course_view(pos, count, []);
                        }
                        else{
                            pos = (parseInt($(this).text())-1)*count;
                            $.session.set('pos',pos);
                            course_view(pos, count, []);
                        }
                    });
                    $('[data-tab=course] .item_in_page').dropdown({
                        onChange: function (value, text, item) {
                            pos = 0;
                            $.session.set('pos',pos);
                            $.session.set('count',value);
                            count = value;
                            course_view(pos, count, []);
                        }
                    });
                    $('[data-tab=course] .cards .image img').one('error', function() { this.src = 'static/images/default.jpg'; });
                    $('[data-tab=course] .enroll').unbind('click');
                    $('[data-tab=course] .enroll').click(function () {
                        button = $(this);
                        if($(this).hasClass('disabled')){
                            return;
                        }
                        course_id = $(this).attr("course_id");
                        $.ajax({
                            url: "course/setData.php",
                            type: "POST",
                            data: {"method": "enroll", "course_id": course_id},
                            success: function (data) {
                                data = JSON.parse(data);
                                if (data.status == "error") {
                                    $.notify(data.msg, "error");
                                }
                                else if (data.status == "success") {
                                    button.removeClass('green').addClass('blue').addClass('disabled').text('Вы уже записаны');
                                    $.notify(data.msg, "success");
                                    label = button.prev();
                                    count_members = parseInt(label.text())+1;
                                    label.text(""+(count_members == null ? 0 : count_members)+" "+
                                        declOfNum((count_members == null ? 0 : count_members), ['участник', 'участника', 'участников']))
                                }
                            },
                            error: function (data) {
                                load_end();
                                $.notify("Произошла ошибка, обратитесь к администратору", "error");
                            }
                        })
                    });
                    $('[data-tab=course] .setting').unbind('click');
                    $('[data-tab=course] .setting').click(function () {
                        if($('.ui.modal.course_settings').length == 0){
                            $.ajax({
                                url: "directory/getData.php",
                                type: "POST",
                                data: {
                                    "method": "getLessons",},
                                success: function (data) {
                                    data = JSON.parse(data);
                                    if (data.status == "error") {
                                        $.notify(data.msg, "error");
                                    }
                                    else if (data.status == "success") {
                                        lesson_options = [];
                                        data.data.map(function (lesson) {
                                            lesson_options.push("<option value='"+lesson.id+"'>"+lesson.shortname+"</option>")
                                        });
                                        $('body').append("<div class='ui modal course_settings'>" +
                                                "<i class='close icon'></i>" +
                                                "<div class='header'>" +
                                                    "Настройка отображения списка курсов" +
                                                "</div>" +
                                                "<div class='content'>" +
                                                    "<div class='description'>" +
                                                        "<table>" +
                                                            "<tr>" +
                                                                "<td>Сортировка</td>" +
                                                                "<td><select class='ui dropdown sort'><option value='0'>Без сортировки</option><option value='rating'>По рейтингу</option></select></td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>Дисциплина</td>" +
                                                                "<td><select class='ui dropdown lesson'><option value='0'>Все</option>"+lesson_options+"</select></td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>Название</td>" +
                                                                "<td><div class='ui input'><input type='text' class='name'></div></td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>Только свои</td>" +
                                                                "<td><div class='ui toggle checkbox onlyMy'><input type='checkbox'></div></td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>Отобразить старые</td>" +
                                                                "<td><div class='ui toggle checkbox old'><input type='checkbox'></div></td>" +
                                                            "</tr>" +
                                                        "</table>" +
                                                    "</div>" +
                                                "</div>" +
                                                "<div class='actions'>" +
                                                    "<div class='ui black deny button'>Сбросить настройки</div>" +
                                                    "<div class='ui positive right labeled icon button'>Применить<i class='checkmark icon'></ic></div>" +
                                                "</div>" +
                                            "</div>");
                                        $('.ui.modal.course_settings').modal({autofocus: false,
                                        onApprove: function () {
                                            course_settings.sort = $('.ui.modal.course_settings .sort').dropdown('get value');
                                            course_settings.lesson = $('.ui.modal.course_settings .lesson').dropdown('get value');
                                            course_settings.name = $('.ui.modal.course_settings .name').val();
                                            course_settings.onlyMy = $('.ui.modal.course_settings .onlyMy').checkbox('is checked');
                                            course_settings.old = $('.ui.modal.course_settings .old').checkbox('is checked');
                                            $.cookie('course_settings',JSON.stringify(course_settings));
                                            course_view(0,count);
                                        },
                                        onDeny: function () {
                                            course_settings = {
                                                sort: 0,
                                                lesson: 0,
                                                name: '',
                                                onlyMy: false,
                                                old: false
                                            };
                                            $.cookie('course_settings',JSON.stringify(course_settings));
                                            course_view(0,count);
                                        }}).modal('show');
                                        $('.ui.modal.course_settings .ui.dropdown').dropdown();
                                        $('.ui.modal.course_settings .ui.checkbox').checkbox();
                                        $('.ui.modal.course_settings .sort').dropdown('set selected',course_settings.sort);
                                        $('.ui.modal.course_settings .lesson').dropdown('set selected',course_settings.lesson);
                                        $('.ui.modal.course_settings .name').val(course_settings.name);
                                        $('.ui.modal.course_settings .onlyMy').checkbox(course_settings.onlyMy == true ? 'check' : 'uncheck');
                                        $('.ui.modal.course_settings .old').checkbox(course_settings.old == true ? 'check' : 'uncheck');

                                    }
                                },
                                error: function (data) {

                                    $.notify("Произошла ошибка, обратитесь к администратору", "error");
                                }
                            });
                        }
                        else{
                            $('.ui.modal.course_settings').modal('show')
                        }
                    })
                }
                else {
                    $.notify("Произошла ошибка, обратитесь к администратору", "error");
                }
            },
            error: function (data) {
                load_end();
                $.notify("Произошла ошибка, обратитесь к администратору", "error");
            }

        })
    }
});



function generatePages(count, checked) {
    result = [];
    var j = 1 + (checked <= 2 || (checked > count - 3) ? 0 : checked - 2 - (checked == count - 3));
    var i = 1,l = count > 7 ? 7 : count;
    while(i <= l){
        if(i > 3 && count > 7){
            if(i == 4){
                result.push('<a class="item disabled">...</a>');
            }
            else{
                k = count - (l - i);
                result.push("<a class='item "+(checked == k ? "active" : "")+"'>"+k+"</a>");
            }

        }
        else{
            k = j;
            result.push("<a class='item "+(checked == k ? "active" : "")+"'>"+k+"</a>");
        }
        i++; j++;
    }
    return result.join("");
}
function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
}